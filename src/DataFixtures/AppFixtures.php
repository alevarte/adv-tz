<?php

namespace App\DataFixtures;

use App\Entity\Product;
use App\Entity\Tag;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    private $tags;

    public function __construct()
    {
        $this->tags = $this->getTagData();
    }

    public function load(ObjectManager $manager): void
    {
        $this->loadTags($manager);
        $this->loadProducts($manager);
    }

    private function loadTags(ObjectManager $manager): void
    {
        foreach ($this->tags as $index => $name) {
            $tag = new Tag();
            $tag->setName($name);

            $manager->persist($tag);

            $this->addReference('tag-' . $name, $tag);
        }

        $manager->flush();
    }

    private function loadProducts(ObjectManager $manager): void
    {
        foreach ($this->loadProductData() as [$title, $price, $quantity, $tags]) {
            $product = new Product();

            $product->setTitle($title);
            $product->setPrice($price);
            $product->setQuantity($quantity);
            $product->addTag(...$tags);

            $manager->persist($product);
        }

        $manager->flush();
    }

    private function getTagData(): array
    {
        $tags = [];

        for ($i = 0; $i < 10; $i++) {
            $tags[] = 'tag ' . $i;
        }

        return $tags;
    }

    private function loadProductData()
    {
        $products = [];
        for ($i = 0; $i < 10; $i++) {
            $products[] = [
                'product ' . $i,
                mt_rand(10, 100),
                mt_rand(1, 10),
                $this->getRandomTags(),
            ];
        }

        return $products;
    }

    private function getRandomTags(): array
    {
        shuffle($this->tags);
        $selectedTags = \array_slice($this->tags, 0, random_int(2, 4));

        return array_map(function ($tagName) {
            return $this->getReference('tag-' . $tagName);
        }, $selectedTags);
    }
}
