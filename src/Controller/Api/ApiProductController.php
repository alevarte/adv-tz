<?php


namespace App\Controller\Api;


use App\Entity\Product;
use App\Entity\Tag;
use App\Repository\ProductRepository;
use App\Repository\TagRepository;
use App\Service\ProductService;
use JMS\Serializer\SerializerBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @Route("/product")
 */
class ApiProductController extends AbstractController
{
    private $serializer;

    public function __construct()
    {
        $this->serializer = SerializerBuilder::create()->build();
    }

    /**
     * @Route("/add", name="api_product_add",  methods={"POST"})
     * @param Request $request
     * @param TagRepository $tags
     * @param ProductService $productService
     * @return JsonResponse|RedirectResponse
     */
    public function addProduct(Request $request, TagRepository $tags, ProductService $productService)
    {
        $data = json_decode(
            $request->getContent(),
            true
        );

        $validator = Validation::createValidator();

        $constraint = new Assert\Collection(array(
            'title' => new Assert\Length(array('min' => 1)),
            'quantity' => new Assert\Type(array('type' => 'numeric')),
            'price' => new Assert\Type(array('type' => 'numeric')),
            'tags' => new Assert\Type(array('type' => 'array')),
        ));


        try {

            $violations = $validator->validate($data, $constraint);
            if ($violations->count() > 0) {
                throw new \Exception('Invalid data', 500);
            }

            $tagArr = [];
            foreach ($data['tags'] as $tagId) {
                $tag = $tags->find($tagId);
                if (!$tag instanceof Tag) {
                    throw new NotFoundHttpException("tag {$tagId} not found", null, 404);
                }
                $tagArr[] = $tag;
            }

            $productId = $productService->createProduct(
                $data['title'],
                $data['price'],
                $data['quantity'],
                $tagArr
            );

        } catch (\Exception $e) {
            return new JsonResponse($this->serializer->serialize([
                'error' => $e->getMessage()
            ], 'json'), 500);
        }

        return new JsonResponse($this->serializer->serialize([
            "success" => true,
            "product_id" => $productId
        ], 'json'), 200);
    }

    /**
     * @Route("/get/{product_id}", name="api_product_get",  methods={"GET"})
     * @param Request $request
     * @param ProductRepository $products
     * @param int $product_id
     * @return string
     */
    public function getProduct(Request $request, ProductRepository $products, int $product_id)
    {
        try {

            $product = $products->find($product_id);
            if (!$product instanceof Product) {
                throw new NotFoundHttpException("product_id {$product_id} not found", null, 404);
            }

        } catch (\Exception $e) {
            return new JsonResponse($this->serializer->serialize([
                "error" => $e->getMessage(),
            ], 'json'), $e->getCode());
        }

        return new JsonResponse($this->serializer->serialize([
            "success" => true,
            "product" => $product
        ], 'json'), 200);
    }

    /**
     * @Route("/update", name="api_product_update",  methods={"PUT"})
     * @param Request $request
     * @param ProductRepository $products
     * @param TagRepository $tags
     * @param ProductService $productService
     * @return JsonResponse|RedirectResponse
     */
    public function updateProduct(Request $request, ProductRepository $products, TagRepository $tags, ProductService $productService)
    {
        $data = json_decode(
            $request->getContent(),
            true
        );

        $validator = Validation::createValidator();

        $constraint = new Assert\Collection(array(
            'product_id' => new Assert\Type(array('type' => 'numeric')),
            'title' => new Assert\Length(array('min' => 1)),
            'quantity' => new Assert\Type(array('type' => 'numeric')),
            'price' => new Assert\Type(array('type' => 'numeric')),
            'tags' => new Assert\Type(array('type' => 'array')),
        ));

        try {

            $violations = $validator->validate($data, $constraint);
            if ($violations->count() > 0) {
                throw new \Exception("Invalid data", 500);
            }

            $product = $products->find($data['product_id']);
            if (!$product instanceof Product) {
                throw new NotFoundHttpException("Product not found", null, 404);
            }

            $tagArr = [];
            foreach ($data['tags'] as $tagId) {
                $tag = $tags->find($tagId);
                if (!$tag instanceof Tag) {
                    throw new NotFoundHttpException("tag {$tagId} not found", null, 404);
                }
                $tagArr[] = $tag;
            }

            $productService->updateProduct(
                $product,
                $data['title'],
                $data['price'],
                $data['quantity'],
                $tagArr
            );

        } catch (\Exception $e) {
            return new JsonResponse($this->serializer->serialize([
                "error" => $e->getMessage(),
            ], 'json'), 500);
        }

        return new JsonResponse($this->serializer->serialize([
            "success" => true,
        ], 'json'), 200);
    }

    /**
     * @Route("/remove", name="api_product_remove",  methods={"DELETE"})
     * @param Request $request
     * @param ProductRepository $products
     * @param ProductService $productService
     * @return JsonResponse|RedirectResponse
     */
    public function removeProduct(Request $request, ProductRepository $products, ProductService $productService)
    {
        $data = json_decode(
            $request->getContent(),
            true
        );

        $validator = Validation::createValidator();

        $constraint = new Assert\Collection(array(
            'product_id' => new Assert\Type(array('type' => 'numeric')),
        ));

        try {

            $violations = $validator->validate($data, $constraint);
            if ($violations->count() > 0) {
                throw new \Exception("Invalid data", 500);
            }

            $product = $products->find($data['product_id']);
            if (!$product instanceof Product) {
                throw new NotFoundHttpException("Product not found", null, 404);
            }

            $productService->removeProduct($product);

        } catch (\Exception $e) {
            return new JsonResponse($this->serializer->serialize([
                "error" => $e->getMessage(),
            ], 'json'), 500);
        }

        return new JsonResponse($this->serializer->serialize([
            "success" => true,
        ], 'json'), 200);
    }
}