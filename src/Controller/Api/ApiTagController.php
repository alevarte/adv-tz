<?php


namespace App\Controller\Api;


use App\Entity\Tag;
use App\Repository\TagRepository;
use App\Service\TagService;
use JMS\Serializer\SerializerBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @Route("/tag")
 */
class ApiTagController extends AbstractController
{
    private $serializer;

    public function __construct()
    {
        $this->serializer = SerializerBuilder::create()->build();
    }

    /**
     * @Route("/add", name="api_tag_add",  methods={"POST"})
     * @param Request $request
     * @param TagService $tagService
     * @return JsonResponse|RedirectResponse
     */
    public function addTag(Request $request, TagService $tagService)
    {
        $data = json_decode(
            $request->getContent(),
            true
        );

        $validator = Validation::createValidator();

        $constraint = new Assert\Collection(array(
            'name' => new Assert\Length(array('min' => 1)),
        ));

        try {

            $violations = $validator->validate($data, $constraint);
            if ($violations->count() > 0) {
                throw new \Exception('Invalid data', 500);
            }

            $tagId = $tagService->createTag(
                $data['name']
            );

        } catch (\Exception $e) {
            return new JsonResponse($this->serializer->serialize([
                'error' => $e->getMessage()
            ], 'json'), 500);
        }

        return new JsonResponse($this->serializer->serialize([
            "success" => true,
            "tagId" => $tagId
        ], 'json'), 200);
    }

    /**
     * @Route("/get/{tag_id}", name="api_tag_get",  methods={"GET"})
     * @param Request $request
     * @return JsonResponse|RedirectResponse
     */
    public function getTag(Request $request, TagRepository $tags, int $tag_id)
    {
        try {

            $tag = $tags->find($tag_id);
            if (!$tag instanceof Tag) {
                throw new NotFoundHttpException("tag_id {$tag_id} not found", null, 404);
            }

        } catch (\Exception $e) {
            return new JsonResponse($this->serializer->serialize([
                "error" => $e->getMessage(),
            ], 'json'), $e->getCode());
        }

        return new JsonResponse($this->serializer->serialize([
            "success" => true,
            "tag" => $tag
        ], 'json'), 200);
    }

    /**
     * @Route("/update", name="api_tag_update",  methods={"PUT"})
     * @param Request $request
     * @return JsonResponse|RedirectResponse
     */
    public function updateTag(Request $request, TagRepository $tagRepository, TagService $tagService)
    {
        $data = json_decode(
            $request->getContent(),
            true
        );

        $validator = Validation::createValidator();

        $constraint = new Assert\Collection(array(
            'tag_id' => new Assert\Type(array('type' => 'numeric')),
            'name' => new Assert\Length(array('min' => 1)),
        ));

        try {

            $violations = $validator->validate($data, $constraint);
            if ($violations->count() > 0) {
                throw new \Exception("Invalid data", 500);
            }

            $tag = $tagRepository->find($data['tag_id']);
            if (!$tag instanceof Tag) {
                throw new NotFoundHttpException("Tag not found", null, 404);
            }

            $tagService->updateTag(
                $tag,
                $data['name']
            );

        } catch (\Exception $e) {
            return new JsonResponse($this->serializer->serialize([
                "error" => $e->getMessage(),
            ], 'json'), 500);
        }

        return new JsonResponse($this->serializer->serialize([
            "success" => true,
        ], 'json'), 200);
    }

    /**
     * @Route("/remove", name="api_tag_remove",  methods={"DELETE"})
     * @param Request $request
     * @return JsonResponse|RedirectResponse
     */
    public function removeTag(Request $request, TagRepository $tags, TagService $tagService)
    {
        $data = json_decode(
            $request->getContent(),
            true
        );

        $validator = Validation::createValidator();

        $constraint = new Assert\Collection(array(
            'tag_id' => new Assert\Type(array('type' => 'numeric')),
        ));

        try {

            $violations = $validator->validate($data, $constraint);
            if ($violations->count() > 0) {
                throw new \Exception("Invalid data", 500);
            }

            $tag = $tags->find($data['tag_id']);
            if (!$tag instanceof Tag) {
                throw new NotFoundHttpException("Tag not found", null, 404);
            }

            $tagService->removeTag($tag);

        } catch (\Exception $e) {
            return new JsonResponse($this->serializer->serialize([
                "error" => $e->getMessage(),
            ], 'json'), 500);
        }

        return new JsonResponse($this->serializer->serialize([
            "success" => true,
        ], 'json'), 200);
    }
}