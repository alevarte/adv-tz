<?php

namespace App\Service;

use App\Entity\Tag;
use \Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Class TagService
 * @package App\Service
 */
class TagService
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    protected $doctrine;

    protected $manager;


    /**
     * TagService constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->doctrine = $this->container->get('doctrine');
        $this->manager = $this->doctrine->getManager();
    }


    /**
     * @param string $name
     * @return int|null
     */
    public function createTag(string $name)
    {
        $tag = new Tag();
        $tag->setName($name);

        $this->manager->persist($tag);
        $this->manager->flush();

        return $tag->getId();
    }

    /**
     * @param Tag $tag
     * @param string $name
     * @return bool
     */
    public function updateTag(Tag $tag, string $name)
    {
        $tag->setName($name);

        $this->manager->flush();

        return true;
    }

    /**
     * @param Tag $tag
     * @return bool
     */
    public function removeTag(Tag $tag)
    {
        $this->manager->remove($tag);
        $this->manager->flush();

        return true;
    }
}