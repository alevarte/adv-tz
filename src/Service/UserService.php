<?php

namespace App\Service;

use App\Entity\Product;
use \Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Class ProductService
 * @package App\Service
 */
class UserService
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    protected $doctrine;

    protected $manager;


    /**
     * ProductService constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->doctrine = $this->container->get('doctrine');
        $this->manager = $this->doctrine->getManager();
    }


    /**
     * @param string $title
     * @param float $price
     * @param int $quantity
     * @param array $tags
     * @return int|null
     */
    public function createProduct(string $title, float $price, int $quantity, array $tags = [])
    {
        $product = new Product();

        $product->setTitle($title);
        $product->setPrice($price);
        $product->setQuantity($quantity);
        $product->addTag(...$tags);

        $this->manager->persist($product);
        $this->manager->flush();

        return $product->getId();
    }

    /**
     * @param Product $product
     * @param string $title
     * @param float $price
     * @param int $quantity
     * @param array $tags
     * @return bool
     */
    public function updateProduct(Product $product, string $title, float $price, int $quantity, array $tags = [])
    {
        $product->setTitle($title);
        $product->setPrice($price);
        $product->setQuantity($quantity);
        $product->addTag(...$tags);

        $this->manager->flush();

        return true;
    }

    /**
     * @param Product $product
     * @return bool
     */
    public function removeProduct(Product $product)
    {
        $this->manager->remove($product);
        $this->manager->flush();

        return true;
    }
}