<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190825093903 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add tags';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("
            INSERT INTO `tag` (`name`)
            VALUES
                ('NEW'),
                ('SALE'),
                ('DISCONT');
        ");

    }

    public function down(Schema $schema): void
    {
        $this->addSql("
            DELETE FROM `tag`
            WHERE `name` IN ('NEW', 'SALE', 'DISCONT');
        ");
    }
}
